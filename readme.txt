The following features has been implemented:

Create an Android app that displays recipes.
The data can be obtained from https://g525204.github.io/recipes.json.
In the first screen of the app, display a list of the recipes loaded from the API.
Some of the recipes returned by the API will have an image, some won't.
The list should contain two sections
- the first section will display recipes that have images, with the header "Popular";
- while the second section will display recipes that do not have images, with the header "Other".
The two sections should scroll as a single scroll container.
The first section will display each recipe as a tile, with the image filling the entire tile.
The title of the recipe should be overlaid on the image, aligned to the bottom right corner of the tile.
It should wrap if it is too long to fit in one line.
This section should display two columns of tiles when the device is in portrait mode,
or three columns when it is in landscape mode.

The second section will simply display each recipe's title, left-aligned as a list item with no image.
When the user taps on one of the items, perform a slide-left transition to a recipe details screen that displays the recipe's image, title, ingredients.
You can lay out this screen in any way you like, with whatever UI components you choose, as long as it displays all the information clearly.
When this screen transitions back to the first screen, it should be a slide-right transition.
The recipe details screen should also display the URL of the recipe as a clickable link (not a button).
When the link is clicked, open the URL in an in-app browser.

