package com.kuchar.recipe.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ErrorResponseTest {

    @Test
    public void testBuild() {
        final String MSG = "error message";
        ErrorResponse errorResponse = ErrorResponse.build(new Throwable(MSG));
        assertEquals(MSG, errorResponse.getMessage());
    }
}
