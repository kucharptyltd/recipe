package com.kuchar.recipe.services;

import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;

import com.kuchar.recipe.MainActivity;
import com.kuchar.recipe.RecipeAdapter;
import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.models.RecipeData;
import com.kuchar.recipe.services.recipes.RecipeClient;
import com.kuchar.recipe.view.RecipeItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {

    private MainActivity mainActivity;
    private ActivityController<MainActivity> controller;

    @Mock
    private RecipeClient recipeClient;

    @Mock
    private RecyclerView recyclerView;

    @Mock
    private RecipeAdapter adapter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        controller = Robolectric.buildActivity(MainActivity.class).create();
        mainActivity = controller.get();

        mainActivity.setRecipeClient(recipeClient);
        mainActivity.setRecyclerView(recyclerView);
        mainActivity.setAdapter(adapter);
    }

    @Test
    public void testGetRecipes() throws Exception {

        List<Recipe> recipes = new ArrayList<>(4);
        recipes.add(new Recipe("Mock1 Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));
        recipes.add(new Recipe("Mock2 Savoury Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));
        recipes.add(new Recipe("Mock3 Deviled Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs",""));
        recipes.add(new Recipe("Mock4 Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));

        RecipeData<Recipe> recipeData = new RecipeData<Recipe>("Mock Recipe Puppy","0.1",
                "http:\\/\\/www.recipepuppy.com\\/",recipes);


        when(recipeClient.getRecipes())
                .thenReturn(rx.Observable.just(recipeData));

        controller.start();


        Mockito.verify(recipeClient)
                .getRecipes();

        Mockito.verify(adapter)
                .notifyDataSetChanged();

        assertTrue(mainActivity.getListItems().size() == 6);
        assertTrue(mainActivity.getListItems().get(0).getItemViewType() == 0);
        assertTrue(mainActivity.getListItems().get(4).getItemViewType() == 0);
        assertTrue(mainActivity.getListItems().get(1).getItemViewType() == 1);
        assertTrue(((RecipeItem)mainActivity.getListItems().get(5)).getRecipe().getThumbnail().isEmpty());
    }

    @Test
    public void testGetRecipesReturnsOneRecipe() throws Exception {

        List<Recipe> recipes = new ArrayList<>(4);
        recipes.add(new Recipe("Mock1 Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));

        RecipeData<Recipe> recipeData = new RecipeData<Recipe>("Mock Recipe Puppy","0.1",
                "http:\\/\\/www.recipepuppy.com\\/",recipes);

        when(recipeClient.getRecipes())
                .thenReturn(rx.Observable.just(recipeData));

        controller.start();

        Mockito.verify(recipeClient)
                .getRecipes();

        Mockito.verify(adapter)
                .notifyDataSetChanged();

        assertTrue(mainActivity.getListItems().size() == 3);
        assertTrue(mainActivity.getListItems().get(0).getItemViewType() == 0);
        assertTrue(mainActivity.getListItems().get(2).getItemViewType() == 0);
        assertTrue(mainActivity.getListItems().get(1).getItemViewType() == 1);
    }

    @Test
    public void testGetRecipesError() throws Exception {

        when(recipeClient.getRecipes())
                .thenThrow(new RuntimeException("Service not available"));
        try {
            controller.start();
        } catch (RuntimeException e){
            Mockito.verify(recipeClient)
                    .getRecipes();
            Mockito.verifyZeroInteractions(adapter);
        }

    }


    @Test
    public void testNumOfColumns() throws Exception {

        List<Recipe> recipes = new ArrayList<>(4);
        recipes.add(new Recipe("Mock1 Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));
        recipes.add(new Recipe("Mock2 Savoury Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));
        recipes.add(new Recipe("Mock3 Deviled Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs",""));
        recipes.add(new Recipe("Mock4 Eggs",
                "http:\\/\\/www.kraftfoods.com\\/kf\\/recipes\\/savory-deviled-eggs-55779.aspx",
                "eggs","http:\\/\\/img.recipepuppy.com\\/627470.jpg"));

        RecipeData<Recipe> recipeData = new RecipeData<Recipe>("Mock Recipe Puppy","0.1",
                "http:\\/\\/www.recipepuppy.com\\/",recipes);


        when(recipeClient.getRecipes())
                .thenReturn(rx.Observable.just(recipeData));

        controller.start();

        Mockito.verify(recipeClient)
                .getRecipes();

        Mockito.verify(adapter)
                .notifyDataSetChanged();

        if (mainActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            assertTrue(mainActivity.getLayoutManager().getSpanCount() == 3);
        } else {
            assertTrue(mainActivity.getLayoutManager().getSpanCount() == 2);
        }
    }

}
