package com.kuchar.recipe.services.recipes;

import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.models.RecipeData;

import rx.Observable;

public interface RecipeClient {

    Observable<RecipeData<Recipe>> getRecipes();

}


