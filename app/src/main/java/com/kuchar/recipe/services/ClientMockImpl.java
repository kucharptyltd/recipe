package com.kuchar.recipe.services;

import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.models.RecipeData;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * ClientMockImpl
 * Mock client can be used for local testing when server or internet is not available.
 *
 */
public class ClientMockImpl {

    private RecipeData<Recipe> recipeData;

    public ClientMockImpl() {
        List<Recipe> recipes = new ArrayList<>(8);
        recipes.add(new Recipe("Mock1 Eggs",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs","http://img.recipepuppy.com/627470.jpg"));
        recipes.add(new Recipe("Mock2 Eggs",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs","http://img.recipepuppy.com/627470.jpg"));
        recipes.add(new Recipe("Mock3 Eggs",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs",""));
        recipes.add(new Recipe("Mock4 Eggs NT",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs","http://img.recipepuppy.com/627470.jpg"));
        recipes.add(new Recipe("Mock5 Eggs NT",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs",""));
        recipes.add(new Recipe("Mock6 Savoury Eggs",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs","http://img.recipepuppy.com/627470.jpg"));
        recipes.add(new Recipe("Mock7 Deviled Eggs",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs","http://img.recipepuppy.com/627470.jpg"));
        recipes.add(new Recipe("Mock8 Eggs NT",
                "http://www.kraftfoods.com/kf/recipes/savory-deviled-eggs-55779.aspx",
                "eggs",""));

        this.recipeData = new RecipeData<Recipe>("Mock Recipe Puppy","0.1",
                "http://www.recipepuppy.com/",recipes);
    }

    public RecipeData<Recipe> getRecipeData() {
        return recipeData;
    }

    public void sleep(long ms) {
        try {
            Thread.sleep(ms);
        }
        catch (InterruptedException ignored) {
        }
    }
}
