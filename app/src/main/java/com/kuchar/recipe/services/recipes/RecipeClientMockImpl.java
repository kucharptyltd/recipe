package com.kuchar.recipe.services.recipes;

import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.services.ClientMockImpl;
import com.kuchar.recipe.models.RecipeData;

import rx.Observable;
import rx.Subscriber;

/**
 *
 * RecipeClientMockImpl
 * Mock client can be used for local testing when server or internet is not available.
 *
 */
public class RecipeClientMockImpl extends ClientMockImpl implements RecipeClient {
    @Override
    public Observable<RecipeData<Recipe>> getRecipes() {
        return Observable.create(new Observable.OnSubscribe<RecipeData<Recipe>>() {

            @Override
            public void call(Subscriber<? super RecipeData<Recipe>> subscriber) {
                sleep(1000l);
                subscriber.onNext(new RecipeData<Recipe>(getRecipeData()));
            }

        });
    }
}
