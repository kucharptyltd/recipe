package com.kuchar.recipe.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kuchar.recipe.util.Environment;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * ClientNetworkImpl is a client which builds retrofit RestAdapter.
 */
public class ClientNetworkImpl {

    protected <T> T securedApi(Class<T> apiInterface) {
        return getRestAdapter(true).create(apiInterface);
    }

    protected <T> T securedApi(String baseUrl, Class<T> apiInterface) {
        return getRestAdapter(baseUrl, true).create(apiInterface);
    }

    protected <T> T unsecuredApi(Class<T> apiInterface) {
        return getRestAdapter(false).create(apiInterface);
    }

    protected <T> T unsecuredApi(String baseUrl, Class<T> apiInterface) {
        return getRestAdapter(baseUrl, false).create(apiInterface);
    }

    protected RestAdapter getRestAdapter(boolean secured) {
        String baseUrl = Environment.PROD;
        return getRestAdapter(baseUrl, secured);
    }

    protected RestAdapter getRestAdapter(final String baseUrl, final boolean secured) {
        return new RestAdapter.Builder()
                .setEndpoint(baseUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson()))
                .build();
    }

    private Gson gson() {
        return new GsonBuilder()
                .create();
    }

}
