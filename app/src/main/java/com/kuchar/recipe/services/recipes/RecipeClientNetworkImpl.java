package com.kuchar.recipe.services.recipes;

import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.services.ClientNetworkImpl;
import com.kuchar.recipe.models.RecipeData;

import retrofit.http.GET;
import rx.Observable;

/**
 * RecipeClientNetworkImpl
 * Recipe client allows to make getRecipes call using retrofit and rx android.
 */
public class RecipeClientNetworkImpl extends ClientNetworkImpl implements RecipeClient {

    @Override
    public Observable<RecipeData<Recipe>> getRecipes() {
        return api().getRecipes();
    }

    // ---- api -----zs----------------------------------------------------------------------------

    protected Api api() {
        return securedApi(Api.class);
    }


    public interface Api {

        @GET("/recipes.json")
        public Observable<RecipeData<Recipe>> getRecipes();

    }

}
