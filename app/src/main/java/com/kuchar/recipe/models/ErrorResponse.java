package com.kuchar.recipe.models;

/**
 * ErrorResponse
 * Data class
 */
public class ErrorResponse {

    protected Throwable throwable;
    protected int statusCode;
    protected String type;
    protected String message;

    public static ErrorResponse build(Throwable e) {
        //TODO: Handle different error scenarios
        return new ErrorResponse(e, 500, e.getMessage());
    }

    public ErrorResponse() {
    }

    public ErrorResponse(Throwable throwable, int statusCode, String message) {
        this.throwable = throwable;
        this.statusCode = statusCode;
        this.message = message;
    }

    public ErrorResponse(Throwable throwable, int statusCode, String type, String message) {
        this.throwable = throwable;
        this.statusCode = statusCode;
        this.type = type;
        this.message = message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}