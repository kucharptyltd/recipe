package com.kuchar.recipe.models;

import java.util.List;

/**
 * RecipeData
 * Data class
 * @param <T>
 */
public class RecipeData<T> {
    private String title;
    private String version;
    private String href;
    private List<T> results;

    public RecipeData(String title, String version, String href, List<T> results) {
        this.title = title;
        this.version = version;
        this.href = href;
        this.results = results;
    }

    public RecipeData(RecipeData recipeData) {
        if (recipeData != null) {
            this.title = recipeData.getTitle();
            this.version = recipeData.getVersion();
            this.href = recipeData.getHref();
            this.results = recipeData.getResults();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }
}
