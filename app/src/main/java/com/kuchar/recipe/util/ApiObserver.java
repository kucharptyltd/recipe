package com.kuchar.recipe.util;

import com.kuchar.recipe.models.ErrorResponse;

import rx.Observer;

/**
 * ApiObserver is a template to override success and error cases.
 * @param <T>
 */
public abstract class ApiObserver<T> implements Observer<T> {

    @Override
    public final void onNext(T t) {
        onSuccess(t);
        onCompleted();
    }

    public abstract void onSuccess(T t);

    @Override
    public final void onError(Throwable e) {
        onError(ErrorResponse.build(e));
        onCompleted();
    }

    public abstract void onError(ErrorResponse errorResponse);

}
