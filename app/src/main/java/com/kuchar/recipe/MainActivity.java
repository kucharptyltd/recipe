package com.kuchar.recipe;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.services.recipes.RecipeClient;
import com.kuchar.recipe.services.recipes.RecipeClientNetworkImpl;
import com.kuchar.recipe.util.ApiObserver;
import com.kuchar.recipe.models.ErrorResponse;
import com.kuchar.recipe.models.RecipeData;
import com.kuchar.recipe.view.HeaderItem;
import com.kuchar.recipe.view.ListItem;
import com.kuchar.recipe.view.RecipeItem;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *  Main Activity presents list of recipes using RecyclerView adapter and GridLayoutManager.
 *
 */
public class MainActivity extends AppCompatActivity {

    public static final String POPULAR = "Popular";
    public static final String OTHERS = "Others";
    public static final int FIRST_HEADER_POSITION = 0;
    public static final int COLUMNS_NUM_LANDSCAPE = 3;
    public static final int COLUMNS_NUM_PORTRAIT = 2;

    private RecyclerView recyclerView;
    private RecipeClient recipeClient;
    private RecipeAdapter adapter;
    private List<ListItem> listItems;
    private GridLayoutManager layoutManager;
    private int numColumns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.list);

        numColumns = getNumColumns(getResources().getConfiguration().orientation);
        layoutManager = new GridLayoutManager(this, numColumns);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecipeAdapter(this, new ArrayList<ListItem>());
        recyclerView.setAdapter(adapter);

//        recipeClient = new RecipeClientMockImpl(); //used in dev for local testing
        recipeClient = new RecipeClientNetworkImpl();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getRecipes();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        numColumns = getNumColumns(newConfig.orientation);
        layoutManager.setSpanCount(numColumns);
        super.onConfigurationChanged(newConfig);
    }

    private int getNumColumns(int orientation) {
        return orientation == Configuration.ORIENTATION_LANDSCAPE ? COLUMNS_NUM_LANDSCAPE : COLUMNS_NUM_PORTRAIT;
    }

    public void getRecipes() {

        recipeClient.getRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ApiObserver<RecipeData<Recipe>>() {

                    @Override
                    public void onCompleted() {
                        Log.i("", "completed");
                    }

                    @Override
                    public void onSuccess(RecipeData<Recipe> recipeData) {
                        updateUI(recipeData);
                    }

                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        Log.e("", errorResponse.getMessage());
                    }
                });
    }

    public void updateUI(RecipeData<Recipe> recipeData) {
        List<Recipe> recipes = recipeData.getResults();
        Collections.sort(recipes);
        listItems = new ArrayList<>();
        for(Recipe recipe: recipes){
            listItems.add(RecipeItem.wrap(recipe));
        }
        listItems.add(FIRST_HEADER_POSITION, new HeaderItem(POPULAR));
        int secondHeaderPosition = countRecipesWithImage(recipes) + 1;
        listItems.add(secondHeaderPosition, new HeaderItem(OTHERS));
        adapter.setListItems(listItems);

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup()
        {
            @Override
            public int getSpanSize(int position)
            {
                return isHeader(position) || isRecipeWithEmptyImage(position) ? numColumns : 1;
            }
        });
        adapter.notifyDataSetChanged();
    }

    private boolean isRecipeWithEmptyImage(int position) {
        return listItems.get(position).getItemViewType() == 1
                && ((RecipeItem)listItems.get(position)).getRecipe().getThumbnail().isEmpty();
    }

    private boolean isHeader(int position) {
        return listItems.get(position).getItemViewType() == 0;
    }

    public int countRecipesWithImage(List<Recipe> recipes) {
        int counter = 0;
        for(Recipe recipe: recipes){
            if (StringUtils.isNotEmpty(recipe.getThumbnail())) {
                counter++;
            }
        }
        return counter;
    }


    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public RecipeClient getRecipeClient() {
        return recipeClient;
    }

    public void setRecipeClient(RecipeClient recipeClient) {
        this.recipeClient = recipeClient;
    }

    public RecipeAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecipeAdapter adapter) {
        this.adapter = adapter;
    }

    public List<ListItem> getListItems() {
        return listItems;
    }

    public void setListItems(List<ListItem> listItems) {
        this.listItems = listItems;
    }

    public GridLayoutManager getLayoutManager() {
        return layoutManager;
    }
}
