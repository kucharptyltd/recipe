package com.kuchar.recipe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kuchar.recipe.models.Recipe;
import com.kuchar.recipe.view.HeaderItem;
import com.kuchar.recipe.view.ListItem;
import com.kuchar.recipe.view.RecipeItem;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Recipe Adapter is responsible for creating a ViewHolder object for each item of RecylerView.
 * RecylerView item can be header or recipe item.
 * Binding recipes data from the data source to each item.
 * Inflating each recipes and header item view.
 */
public class RecipeAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<ListItem> listItems;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public RecipeAdapter(Context context, List<ListItem> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(context).inflate(R.layout.custom_view,parent,false);
            return new ViewHolderItem(v);
        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(context).inflate(R.layout.header,parent,false);
            return new ViewHolderHeader(v);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (listItems.get(position).getItemViewType() == 0) {
            ((ViewHolderHeader) holder).textView.setText(((HeaderItem) listItems.get(position)).getHeader());
         } else {
            Recipe recipe = ((RecipeItem) listItems.get(position)).getRecipe();
            if (StringUtils.isNotEmpty(recipe.getThumbnail())) {
                ((ViewHolderItem) holder).textView.setText(recipe.getTitle().trim());
                ((ViewHolderItem) holder).textView2.setText("");
                Picasso.with(context).load(recipe.getThumbnail()).into(((ViewHolderItem) holder).imageView);
            } else {
                ((ViewHolderItem) holder).textView2.setText(recipe.getTitle().trim());
                ((ViewHolderItem) holder).textView.setText("");
                ((ViewHolderItem) holder).imageView.setImageDrawable(null);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listItems.size() > 0?listItems.get(position).getItemViewType():0;
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public void setListItems(List<ListItem> listItems) {
        this.listItems = listItems;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        public TextView textView;
        public TextView textView2;
        public ImageView imageView;
        public RelativeLayout relativeLayout;
        public ViewHolderItem(View v){
            super(v);
            textView = v.findViewById(R.id.tv);
            textView2 = v.findViewById(R.id.tv2);
            imageView = v.findViewById(R.id.img);
            relativeLayout = v.findViewById(R.id.rl);

            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayRecipeDetails();
                }
            });
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        public TextView textView;
        public ViewHolderHeader(View v) {
            super(v);
            textView = v.findViewById(R.id.txtHeader);
        }
    }

     public void displayRecipeDetails() {
        Intent intent = new Intent(context, RecipeDetailsActivity.class);
        intent.putExtra("Recipe", ((RecipeItem)listItems.get(1)).getRecipe());
        context.startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
