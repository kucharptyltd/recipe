package com.kuchar.recipe.view;

import com.kuchar.recipe.models.Recipe;

/**
 * RecipeItem is a View Item which is used to display recipe data.
 */
public class RecipeItem implements ListItem {
    private Recipe recipe;
    private static final int ITEM_VIEW_TYPE = 1;

    public RecipeItem(Recipe recipe) {
        this.recipe = recipe;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public int getItemViewType() {
        return ITEM_VIEW_TYPE;
    }

    public static RecipeItem wrap(Recipe recipe){
        return new RecipeItem(recipe);
    }
}
