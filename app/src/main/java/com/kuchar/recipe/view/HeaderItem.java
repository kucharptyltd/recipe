package com.kuchar.recipe.view;

/**
 * HeaderItem is a View Item which is used to display recipe header.
 */
public class HeaderItem implements ListItem {
    private String header;
    private static final int ITEM_VIEW_TYPE = 0;

    public HeaderItem(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Override
    public int getItemViewType() {
        return ITEM_VIEW_TYPE;
    }
}
