package com.kuchar.recipe.view;

/**
 * ListItem is an interface which is common for header and recipe view items
 */
public interface ListItem {

    int getItemViewType();
}
